numpy==1.19.0
opencv-python-headless==4.2.0.34
pandas==1.1.0
scipy==1.5.2
six==1.15.0
