# Algorithm to find Calibration between Gaze tracker and Web Camera


## Requirements
    python >= 3

    pip install -r requirements.txt

## Data structure

Preferrable data structure is shown as follows (e.g., as in the data directory structure), otherwise you can choose anything. The code is irrespective of the folder structure.
But you need to keep all csv files for camera in one folder and for screen in separate folder. Similarly, for images, keep image folders for camera in one directory.

```bash
data/
└── <UserName>
    ├── csv
    │   ├── Camera
    │   │   └── <Camera-csv-files-here>
    │   └── Screen
    │       └── <Screen-csv-files-here>
    └── images
        ├── Camera
        │   └── <Camera-image-folders-here>
        └── Screen
            └── <Screen-image-folders-here>
```

## Post processing data

   For removing Nans and clustering of camera data, use following command:
   
   -----------------------

   
   **python post_process_data.py <Camera CSV files directory\> <Camera Image folders directory\> <Screen CSV files directory\> <Screen Image folders directory\>**
   
   -----------------------

   
   This will create a folder named 'Label-Camera-Images' and 'Label-Screen-Images' in the respective directories, which can be used for labeling iris points (using MATLAB code).
    


### Calibration Algorithm

   Once you label the iris points, run the following command:
   
   -----------------------

   
   **python main  <Camera CSV files directory\> <Camera Image folders directory\> <Screen CSV files directory\> <Screen Image folders directory\> <JSON file for laptop calibration\> <Type of data you want to visualize\>**
    
   -----------------------

    
   For visualizing "Camera(or Screen) data", replace **<Type of data you want to visualize\>** with **camera (or screen)** other
    
    