import csv
import os
import sys
import pandas as pd

from collections import deque
from shutil import copy2


def unpackCSVData(tobiiDataCSV):
    with open(tobiiDataCSV, mode='r') as csv_file:
        csvReader = csv.reader(csv_file)
        data = []

        for row in csvReader:
            data.append(row)

        return data


def sampleValidData(csvDataDirectory, imageDirectory, exportImageDirectory, exportFileName):
    allTobiiData = []
    bestData = []

    os.makedirs(exportImageDirectory, exist_ok=True)

    for csvFile in os.listdir(csvDataDirectory):
        if not csvFile.endswith(".csv"):
            continue
        print(csvFile)

        tobiiData = unpackCSVData(csvDataDirectory + "/" + csvFile)

        if bestData == []:
            header = tobiiData[0]
            header.append("file_name")

            bestData.append(header)

        for row in tobiiData[1:]:
            row.append(csvFile)
            allTobiiData.append(row)


    for image_folder in os.listdir(imageDirectory):
        if image_folder == '.DS_Store' or os.path.join(imageDirectory, image_folder) == exportImageDirectory:
            continue
        for imageFile in os.listdir(imageDirectory+'/'+image_folder):
            imageName = os.path.splitext(imageFile)[0]
            # print(imageName)

            l_index = bestData[0].index('left_gaze_point_validity')
            r_index = bestData[0].index('right_gaze_point_validity')
            _index = bestData[0].index('system_time_stamp')

            k = 0
            for row in allTobiiData:
                if (row[_index] == imageName) and (row[l_index] == '1') and (row[r_index]=='1'):
                    row[_index] = imageName
                    k += 1
                    # print("OWO")
                    bestData.append(row)
                    copy2(imageDirectory+'/'+image_folder+'/'+imageFile, exportImageDirectory)
                    break

    exportBestData(bestData, exportFileName)


def exportBestData(bestData, exportFileName):
    pd.DataFrame(bestData).to_csv(exportFileName, index=False, header=False)


if __name__ == "__main__":

    _, csvDataDirectory, imageDirectory, exportFileName, exportImageDirectory = sys.argv

    sampleValidData(csvDataDirectory, imageDirectory, exportImageDirectory, exportFileName)
