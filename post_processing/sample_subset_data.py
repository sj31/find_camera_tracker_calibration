import csv
import os
import sys
import pandas as pd
import random
random.seed(20)
from collections import deque
from shutil import copy2


def unpackCSVData(tobiiDataCSV):
    with open(tobiiDataCSV, mode='r') as csv_file:
        csvReader = csv.reader(csv_file)
        data = []

        for row in csvReader:
            data.append(row)

        return data


def sampleSubsetData(csvDataFile, imageDirectory, exportImageDirectory, exportFileName):

    # indices = [random.randint(0, len(os.listdir(imageDirectory))-1) for a in range(15)]

    # files = os.listdir(imageDirectory)

    # for ind in indices:
    #     copy2(imageDirectory + '/' + files[ind], exportImageDirectory)

    allTobiiData = []
    bestData = []

    tobiiData = unpackCSVData(csvDataFile)

    if bestData == []:
        header = tobiiData[0]
        bestData.append(header)

    for row in tobiiData[1:]:
        allTobiiData.append(row)

    for imageFile in os.listdir(imageDirectory):
        if imageFile.endswith(".jpg"):
            imageName = os.path.splitext(imageFile)[0]
            # print(imageName)
            k = 0
            for row in allTobiiData:
                # print(imageName, row[2])
                if str(row[2]) == imageName:
                    print(imageName, row[2])

                    k += 1
                    print("OWO")
                    bestData.append(row)
                    break
            print(k)

    exportBestData(bestData, exportFileName)


def exportBestData(bestData, exportFileName):
    pd.DataFrame(bestData).to_csv(exportFileName, index=False, header=False)


if __name__ == "__main__":
    _, csvDataFile, imageDirectory, exportFileName, exportImageDirectory = sys.argv
    # _, imageDirectory, exportImageDirectory  = sys.argv

    os.makedirs(exportImageDirectory, exist_ok=True)

    bestData = sampleSubsetData(csvDataFile, imageDirectory, exportImageDirectory, exportFileName)
