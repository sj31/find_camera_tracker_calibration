import csv
import os
import sys
import pandas as pd

from collections import deque
from shutil import copy2
from sklearn.cluster import MeanShift, estimate_bandwidth
import numpy as np
import operator
import matplotlib.pyplot as plt


def unpackCSVData(tobiiDataCSV):
    with open(tobiiDataCSV, mode='r') as csv_file:
        csvReader = csv.reader(csv_file)
        data = []

        for row in csvReader:
            data.append(row)

        return data


def computeGazeMeanShift(pastPoints):

    bandwidth = estimate_bandwidth(pastPoints)
    ms = MeanShift(bandwidth=bandwidth, bin_seeding=True).fit(X=pastPoints)
    labels = ms.labels_
    cluster_centers = ms.cluster_centers_

    labels_unique = np.unique(labels)
    n_clusters_ = len(labels_unique)

    dict_label_count = {}
    for lab in labels_unique:
        dict_label_count[lab] = len([i for i in labels if i == lab])

    num_lab_max = max(dict_label_count.values())
    lab_max = max(dict_label_count.items(), key=operator.itemgetter(1))[0]

    validData = deque()
    ind = [i for i in range(len(pastPoints)) if labels[i] == lab_max]

    return ind


def clusterCamData(csvFileDirectory, exportCSVDirectory):

    for csvFile in os.listdir(csvFileDirectory):
        allTobiiData = []
        bestData = []

        if not csvFile.endswith(".csv"):
            continue
        print(csvFile)

        tobiiData = unpackCSVData(csvFileDirectory + '/' + csvFile)

        if bestData == []:
            header = tobiiData[0]
            bestData.append(header)

        for row in tobiiData[1:]:
            allTobiiData.append(row)

        l_index = bestData[0].index('left_gaze_point_validity')
        r_index = bestData[0].index('right_gaze_point_validity')
        right_eye_data_X = bestData[0].index('right_gaze_point_on_display_area_x')
        right_eye_data_Y = bestData[0].index('right_gaze_point_on_display_area_y')
        left_eye_data_X = bestData[0].index('left_gaze_point_on_display_area_x')
        left_eye_data_Y = bestData[0].index('left_gaze_point_on_display_area_y')

        left_clustering_data = []
        right_clustering_data = []
        data_ind = []

        for r in range(len(allTobiiData)):
            row = allTobiiData[r]
            # print(row[l_index])
            if (row[l_index] == '1') and (row[r_index] == '1'):
                left_clustering_data.append([float(row[left_eye_data_X]), float(row[left_eye_data_Y])])
                right_clustering_data.append([float(row[right_eye_data_X]), float(row[right_eye_data_Y])])
                data_ind += [row]

        left_ind = computeGazeMeanShift(left_clustering_data)
        right_ind = computeGazeMeanShift(right_clustering_data)

        common_ind = list(set(left_ind) & set(right_ind))

        for ind in common_ind:
            bestData.append(data_ind[ind])

        exportBestData(bestData, exportCSVDirectory + '/' + csvFile)


def exportBestData(bestData, exportFileName):
    pd.DataFrame(bestData).to_csv(exportFileName, index=False, header=False)


if __name__ == "__main__":
    _, csvDataDirectory, exportCSVDirectory = sys.argv

    os.makedirs(exportCSVDirectory, exist_ok=True)

    clusterCamData(csvDataDirectory, exportCSVDirectory)
