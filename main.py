import cv2
import csv
import os
import random
import sys
from utils.read_csv import read_csv_data
from utils.read_json import read_json_data
import numpy as np
from utils.iris import find_iris
from utils.calibrate import calibrate
from utils.utils import *
from utils.read_screen_data import read_screen_data
from utils.visualize import get_visualizations




def get_paramters(camera_csv_file, screen_csv_file, camLabelDir, screenLabelDir):


    # camera data
    camera_data_left, _ = read_csv_data(camera_csv_file, eye='left')
    camera_data_right, _ = read_csv_data(camera_csv_file, eye='right')

    screen_data_left, fnames_screen = read_csv_data(screen_csv_file, eye='left')
    screen_data_right, _ = read_csv_data(screen_csv_file, eye='right')

    # screen data
    screen_screen_left, screen_screen_right, screen_screen_data = read_screen_data(screenLabelDir)
    camera_screen_left, camera_screen_right, camera_screen_data = read_screen_data(camLabelDir)

    fnames_camera = list(set(list(camera_data_left.keys())).intersection(list(camera_screen_left.keys())))
    fnames_screen = list(set(list(screen_data_left.keys())).intersection(list(screen_screen_left.keys())))

    return camera_data_left, camera_data_right, screen_data_left, screen_data_right, screen_screen_left, \
           screen_screen_right, screen_screen_data, camera_screen_left, camera_screen_right, camera_screen_data, fnames_camera, fnames_screen


def find_calibration_tracker_camera(camera_data_left, camera_data_right, screen_data_left, screen_data_right,
                                    screen_screen_left, screen_screen_right):

    R_g_to_c, T_c = calibrate(camera_data_left, camera_data_right, camera_screen_data, K_matrix, fnames_camera, dist_coeff)
    print('Initial Rotation and Translation are: ', R_g_to_c, T_c)

    for k in range(1000):
        alpha_3d_iris_l, lambda_3d_iris_l, _, _, _, _ = find_iris(screen_data_left, screen_screen_left, fnames_screen,
                                                              K_matrix, K_inv, R_g_to_c, T_c)

        alpha_3d_iris_r, lambda_3d_iris_r, _, _, _, _ = find_iris(screen_data_right, screen_screen_right, fnames_screen,
                                                              K_matrix, K_inv, R_g_to_c, T_c)

        rmat = R_g_to_c.transpose()             # this is equal to Rot_c_to_g
        tvec = -1 * np.matmul(rmat, T_c)   # this is equal to T_g

        data1 = []
        data2 = []

        # fnames = pandas.Series.sort_values(fnames_camera)[5:-5]
        fnames = fnames_camera

        for i in range(len(fnames)):
            ps_vec_left = np.array([camera_screen_data[fnames[i]]['left'][0], camera_screen_data[fnames[i]]['left'][1]])
            ps_vec_right = np.array([camera_screen_data[fnames[i]]['right'][0], camera_screen_data[fnames[i]]['right'][1]])

            gp_left = np.array(camera_data_left[fnames[i]][0])
            gp_right = np.array(camera_data_right[fnames[i]][0])

            data1.append(ps_vec_left)
            data1.append(ps_vec_right)
            data2.append(gp_left.reshape(1, 3))
            data2.append(gp_right.reshape(1, 3))

        for i in range(len(fnames_screen)):
            # labels iris for right and left eye
            ps_vec_left = np.array([screen_screen_data[fnames_screen[i]]['left'][0], screen_screen_data[fnames_screen[i]]['left'][1]])
            ps_vec_right = np.array([screen_screen_data[fnames_screen[i]]['right'][0], screen_screen_data[fnames_screen[i]]['right'][1]])

            # 3d iris points estimated in camera reference frame (Step 1 of algorithm)
            gp_left = np.array(0.5*(alpha_3d_iris_l[fnames_screen[i]][0] + lambda_3d_iris_l[fnames_screen[i]][0]))
            gp_right = np.array(0.5*(alpha_3d_iris_r[fnames_screen[i]][0] + lambda_3d_iris_r[fnames_screen[i]][0]))

            # pe and po in GT frame
            b = np.array(screen_data_left[fnames_screen[i]][1])
            a = np.array(screen_data_left[fnames_screen[i]][0])

            # pe and po in CCS
            b = np.matmul(R_g_to_c, b.reshape(3, 1)) + T_c
            a = np.matmul(R_g_to_c, a.reshape(3, 1)) + T_c
            a = a.reshape(1, 3)[0]
            b = b.reshape(1, 3)[0]
            p = gp_left.reshape(1, 3)[0]
            # finding points for left eye on gaze line closest to estimated 3D iris center
            n = (b - a)
            n /= np.dot(n, n)
            ap = p - a
            t = np.dot(ap, n)
            gp_left = a + t * n  # x is a point on line
            # convert point to GT frame
            gp_left = np.matmul(rmat, gp_left.reshape(3, 1)) + tvec
            data1.append(ps_vec_left)
            data2.append(gp_left.reshape(1, 3))

            # same steps for right eye
            b = np.array(screen_data_right[fnames_screen[i]][1])
            a = np.array(screen_data_right[fnames_screen[i]][0])
            b = np.matmul(R_g_to_c, b.reshape(3, 1)) + T_c
            a = np.matmul(R_g_to_c, a.reshape(3, 1)) + T_c
            a = a.reshape(1, 3)[0]
            b = b.reshape(1, 3)[0]
            p = gp_right.reshape(1, 3)[0]
            n = (b - a)
            n /= np.dot(n, n)
            ap = p - a
            t = np.dot(ap, n)
            gp_right = a + t * n  # x is a point on line
            gp_right = np.matmul(rmat, gp_right.reshape(3, 1)) + tvec
            data1.append(ps_vec_right)
            data2.append(gp_right.reshape(1, 3))


        data1 = np.asarray(data1).astype(np.float32)
        data2 = np.asarray(data2).astype(np.float32)

        _, rvec, T_c, _ = cv2.solvePnPRansac(data2, data1, K_matrix, distCoeffs=dist_coeff, flags=cv2.SOLVEPNP_P3P)

        R_g_to_c = cv2.Rodrigues(rvec)[0]

    return R_g_to_c, T_c


if __name__ == '__main__':

    cv2.setRNGSeed(1337)
    random.seed(1337)
    np.random.seed(1337)

    _, camCSVDir, camImageDir, screenCSVDir, screenImageDir, json_file, type_data = sys.argv

    K_matrix, dist_coeff = np.array(read_json_data(json_file))
    K_inv = np.linalg.inv(K_matrix)

    camImageDir = os.path.join(camImageDir, 'Label-Camera-Images')
    screenImageDir = os.path.join(screenImageDir, 'Label-Screen-Images')
    camCSV_file = os.path.join(camCSVDir, 'Valid_camera_data.csv')
    screenCSV_file = os.path.join(screenCSVDir, 'Valid_screen_data.csv')

    camera_data_left, camera_data_right, screen_data_left, screen_data_right, screen_screen_left, \
    screen_screen_right, screen_screen_data, camera_screen_left, camera_screen_right, camera_screen_data, \
    fnames_camera, fnames_screen = get_paramters(camCSV_file, screenCSV_file, camImageDir, screenImageDir)

    R_g_c, T_C = find_calibration_tracker_camera(camera_data_left, camera_data_right, screen_data_left,
                                                 screen_data_right, screen_screen_left, screen_screen_right)

    print('Calibration from Trakcer to Camera is: \n')
    print('Rotation =')
    print(R_g_c)
    print('Translation =')
    print(T_C)

    if type_data == 'camera':
        get_visualizations(camImageDir, camera_data_left, camera_screen_left, camera_data_right, camera_screen_right,
                           fnames_camera, K_matrix, K_inv, R_g_c, T_C)

    else:
        get_visualizations(screenImageDir, screen_data_left, screen_screen_left, screen_data_right, screen_screen_right,
                           fnames_screen, K_matrix, K_inv, R_g_c, T_C)





