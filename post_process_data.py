
import os
import numpy as np
import random
import sys
from shutil import copy2
import shutil
from post_processing.sample_cluster_camera import clusterCamData
from post_processing.sample_valid_data import sampleValidData
random.seed(1337)


def select_random_images(imageDirectory, exportImageDirectory):
    len_img = len(os.listdir(imageDirectory))
    indices = random.sample(range(len_img), 15)

    files = os.listdir(imageDirectory)

    for ind in indices:
        copy2(imageDirectory + '/' + files[ind], exportImageDirectory)


def post_process_and_sample(camCSVDir, screenCSVDir, cameraImageDir, screenImageDir):

    ### Camera Clustering data ###
    cluster_dir = os.path.join(camCSVDir, 'CameraClusterFiles')
    if os.path.exists(cluster_dir) and os.path.isdir(cluster_dir):
        shutil.rmtree(cluster_dir)
    os.makedirs(cluster_dir)
    print('Clustering camera data and output will be saved at {}'.format(cluster_dir))
    clusterCamData(camCSVDir, cluster_dir)

    ### Camera Validating data - Removing Nans and randomly selecting 15 images for labeling ###

    export_dir = os.path.join(cameraImageDir, 'Valid-Camera-Images')
    if os.path.exists(export_dir) and os.path.isdir(export_dir):
        shutil.rmtree(export_dir)
    os.makedirs(export_dir)
    if os._exists(os.path.join(camCSVDir, 'Valid_camera_data.csv')):
        os.remove(os.path.join(camCSVDir, 'Valid_camera_data.csv'))
    print('Removing Nans from Camera data and saving valid file at {}'.format(export_dir))
    sampleValidData(camCSVDir, cameraImageDir, export_dir, os.path.join(camCSVDir, 'Valid_camera_data.csv'))

    export_dir_subset = os.path.join(cameraImageDir, 'Label-Camera-Images')
    print('Selecting 15 images for labeleing and saving images at {}'.format(export_dir_subset))
    if os.path.exists(export_dir_subset) and os.path.isdir(export_dir_subset):
        shutil.rmtree(export_dir_subset)
    os.makedirs(export_dir_subset)
    select_random_images(export_dir, export_dir_subset)


    ### Screen Validating data - Removing Nans ###
    export_dir = os.path.join(screenImageDir, 'Valid-Screen-Images')
    if os.path.exists(export_dir) and os.path.isdir(export_dir):
        shutil.rmtree(export_dir)
    os.makedirs(export_dir)
    if os._exists(os.path.join(screenCSVDir, 'Valid_screen_data.csv')):
        os.remove(os.path.join(screenCSVDir, 'Valid_screen_data.csv'))
    print('Removing Nans from Screen data and saving valid file at {}'.format(screenCSVDir))
    sampleValidData(screenCSVDir, screenImageDir, export_dir, os.path.join(screenCSVDir, 'Valid_screen_data.csv'))

    export_dir_subset = os.path.join(screenImageDir, 'Label-Screen-Images')
    print('Selecting 15 images for labeleing and saving images at {}'.format(export_dir_subset))
    if os.path.exists(export_dir_subset) and os.path.isdir(export_dir_subset):
        shutil.rmtree(export_dir_subset)
    os.makedirs(export_dir_subset)

    select_random_images(export_dir, export_dir_subset)

    print('Done!!')


if __name__ == '__main__':

    _, camCSVDir, camImageDir, screenCSVDir, screenImageDir = sys.argv

    post_process_and_sample(camCSVDir, screenCSVDir, camImageDir, screenImageDir)










