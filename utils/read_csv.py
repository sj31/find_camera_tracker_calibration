"""
function to read data from csv file
"""

import pandas as pd


def read_csv_data(filename, eye='left'):
    df = pd.read_csv(filename)
    gaze_dict = {}
    fnames = df['system_time_stamp']
    if eye == 'left':
        gaze_origin = df['left_gaze_origin_in_user_coordinate_system']
        gaze_point = df['left_gaze_point_in_user_coordinate_system']
    else:
        gaze_origin = df['right_gaze_origin_in_user_coordinate_system']
        gaze_point = df['right_gaze_point_in_user_coordinate_system']

    for i in range(len(fnames)):
        gaze_o = list(map(float, gaze_origin[i].replace('(', "").replace(')', "").split(",")))
        gaze_p = list(map(float, gaze_point[i].replace('(', "").replace(')', "").split(",")))
        gaze_dict[str(fnames[i])] = (gaze_o, gaze_p)

    return gaze_dict, fnames

