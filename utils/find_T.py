"""
function to find translation vector from gaze tracker to camera
"""
import numpy as np
from scipy import linalg
from sklearn.preprocessing import normalize


def Normalize(x):
    return x / np.linalg.norm(x)


def find_T(R, K, gs, ps, fnames):

    K_inv = np.linalg.inv(K)

    A = np.zeros((3*len(fnames), len(fnames)+3))
    b = np.zeros(3*len(fnames))

    for i in range(len(fnames)):
        try:
            ps_vec = np.array([ps[fnames[i+1]][0], ps[fnames[i+1]][1], 1.0])
            pc_vec = np.matmul(K_inv, ps_vec)

            gc = np.matmul(R, np.array(gs[fnames[i+1]][0]))
            A[3*i:3*(i+1), i] = pc_vec
            b[3*i:3*(i+1)] = gc
            A[3*i:3*(i+1), -3:] = -1*np.eye(3)
        except KeyError:
            continue

    t = linalg.lstsq(A, b)[0][-3:]

    return t



