import csv
import os

from collections import OrderedDict


def read_screen_data(images_path):

    screen_data = OrderedDict()
    screen_data_left = OrderedDict()
    screen_data_right = OrderedDict()
    files_csv = [f for f in os.listdir(images_path) if f.endswith('.csv')]

    for fn in files_csv:
        with open(images_path + '/' + fn, "r") as f:
            reader = csv.reader(f, delimiter="\t")
            screen_ = []
            for i, line in enumerate(reader):
                screen_ += [[float(round(float(line[0].split(',')[1]))), float(round(float(line[0].split(',')[2])))]]

            screen_data_left[fn.split('.')[0]] = screen_[0]
            screen_data_right[fn.split('.')[0]] = screen_[1]

    for f in screen_data_left.keys():
        screen_data[f] = {'left': screen_data_left[f], 'right': screen_data_right[f]}

    return screen_data_left, screen_data_right, screen_data

