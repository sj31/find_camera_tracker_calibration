"""
file to find iris center
"""

import numpy as np
from scipy import linalg


def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)


def angle_between(v1, v2):
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))*(180.0/np.pi)


def find_iris(camera_data, screen_data, fnames, K_matrix, K_inv, R_g_to_c, T_c):

    # assert len(camera_data) == len(screen_data)

    A = np.zeros((3*len(fnames), 2*len(fnames)))
    b = np.zeros(3*len(fnames))

    new_iris_screen_3d = {}
    new_iris_cam_3d = {}
    pred_iris_2d = {}
    onecmopoint = {}
    distribution = {}
    angle = {}

    for i in range(len(fnames)):

        ps_vec = np.array([screen_data[fnames[i]][0], screen_data[fnames[i]][1], 1.0])
        pc_vec = np.matmul(K_inv, ps_vec)

        poc = np.matmul(R_g_to_c, np.array(camera_data[fnames[i]][0]).reshape(3, 1)) + T_c
        pec = np.matmul(R_g_to_c, np.array(camera_data[fnames[i]][1]).reshape(3, 1)) + T_c

        peo = pec - poc
        peo = peo.reshape(1, 3)

        b[3*i:3*(i+1)] = poc.reshape(1, 3)
        A[3*i:3*(i+1), i] = pc_vec
        A[3*i:3*(i+1), i+len(fnames)] = -1.0*peo

    sol = linalg.lstsq(A, b)[0]

    for case in fnames:

        for i in range(len(fnames)):
            try:
                if fnames[i] == case:
                    index = i
                    break
            except KeyError:
                raise KeyError

        alpha1, lambda1 = sol[index], sol[index+len(fnames)]

        poc = np.matmul(R_g_to_c, np.array(camera_data[case][0]).reshape(3, 1)) + T_c
        pec = np.matmul(R_g_to_c, np.array(camera_data[case][1]).reshape(3, 1)) + T_c
        peo = pec - poc
        pg1 = poc + lambda1*peo

        assert pg1.shape == (3, 1)
        distribution[case] = np.mean(lambda1*peo)

        ps_vec = np.array([screen_data[case][0], screen_data[case][1], 1.0])
        pc_vec = np.matmul(K_inv, ps_vec)
        pg2 = alpha1*pc_vec
        pg2 = pg2.reshape(3, 1)
        assert pg2.shape == (3, 1)

        angle[case] = angle_between((poc - pec).reshape(3,), pc_vec.reshape(3,))

        iris_point_cam = (pg1 + pg2)*0.5
        kk = np.matmul(K_matrix, iris_point_cam)
        assert kk.shape == (3, 1)
        kk = kk / kk[-1]
        ix1, iy1 = kk[0], kk[1]

        new_iris_screen_3d[case] = [pg2]
        new_iris_cam_3d[case] = [pg1]
        pred_iris_2d[case] = [ix1, iy1]

        peo = peo / peo[-1]
        const = 1.0 + peo[0]**2 + peo[1]**2
        const = np.sqrt(100.0/const)
        z_1cmo = iris_point_cam[2] + const
        x_1cmo = iris_point_cam[0] + peo[0]*const
        y_1cmo = iris_point_cam[1] + peo[1]*const

        kk = np.matmul(K_matrix, np.array([x_1cmo, y_1cmo, z_1cmo]))
        assert pg2.shape == (3, 1)
        kk = kk / kk[-1]
        x1cmo, y1cmo = kk[0], kk[1]
        onecmopoint[case] = [x1cmo, y1cmo]

    return new_iris_screen_3d, new_iris_cam_3d, pred_iris_2d, onecmopoint, distribution, angle

