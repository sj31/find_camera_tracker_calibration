"""
function to find Rotation matrix from gaze tracker to camera
"""
import numpy as np
import cv2

def Normalize(x):
    return x / np.linalg.norm(x)


def find_R(K, gs, gs_right,  ps, fnames, dist_coeff):

    data1 = []
    data2 = []

    for i in range(len(fnames)):
        try:
            ps_vec_left = np.array([ps[fnames[i]]['left'][0], ps[fnames[i]]['left'][1]])
            ps_vec_right = np.array([ps[fnames[i]]['right'][0], ps[fnames[i]]['right'][1]])

            gp_left = np.array(gs[fnames[i]][0])
            gp_right = np.array(gs_right[fnames[i]][0])

            data1.append(ps_vec_left)
            data1.append(ps_vec_right)

            data2.append(gp_left)
            data2.append(gp_right)

        except KeyError:
            continue

    data1 = np.asarray(data1).astype(np.float32)
    data2 = np.asarray(data2).astype(np.float32)

    _, rvec, tvec, _ = cv2.solvePnPRansac(data2, data1, K, distCoeffs=dist_coeff, flags=cv2.SOLVEPNP_P3P)

    rmat = cv2.Rodrigues(rvec)[0]

    return rmat, tvec

