"""
function to read json file data
"""

import json
import numpy as np

def read_json_data(filename):

    with open(filename) as json_file:
        data = json.load(json_file)
        return np.array(data['camera_matrix']), np.array(data['dist_coeff'])
