""""
calibrate gaze tracker and camera
"""

from utils.find_R import find_R


def calibrate(camera_data_left, camera_data_right, screen_data, K_matrix, fnames, dist_coeff):

    R, T = find_R(K_matrix, camera_data_left, camera_data_right, screen_data, fnames, dist_coeff)

    return R, T

