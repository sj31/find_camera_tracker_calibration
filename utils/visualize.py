
import cv2
from utils.iris import find_iris
import numpy as np


def get_visualizations(path, camera_data_l, screen_data_l, camera_data_r, screen_data_r, fnames_, K_matrix, K_inv, R_g_c, T_c, save_path=None):
    alpha_3d_iris_l, lambda_3d_iris_l, pred_iris_l, onecmo_l, _, _ = find_iris(camera_data_l, screen_data_l, fnames_, K_matrix, K_inv, R_g_c, T_c)
    alpha_3d_iris_r, lambda_3d_iris_r, pred_iris_r, onecmo_r, _, _ = find_iris(camera_data_r, screen_data_r, fnames_, K_matrix, K_inv, R_g_c, T_c)

    for fn in fnames_:
        img = cv2.imread(path + '/' + fn + '.jpg')

        xl, yl = screen_data_l[fn]
        xr, yr = screen_data_r[fn]

        # draw true iris labels
        cv2.drawMarker(img, (int(xl), int(yl)), color=(255, 255, 255), markerType=cv2.MARKER_CROSS, markerSize=4,
                       thickness=1, line_type=cv2.LINE_AA
                       )
        cv2.drawMarker(img, (int(xr), int(yr)), color=(255, 255, 255), markerType=cv2.MARKER_CROSS, markerSize=4,
                       thickness=1, line_type=cv2.LINE_AA
                       )

        # draw predicted iris labels
        cv2.drawMarker(img, (int(pred_iris_l[fn][0]), int(pred_iris_l[fn][1])), color=(0, 0, 255), markerType=cv2.MARKER_CROSS, markerSize=4,
                       thickness=1, line_type=cv2.LINE_AA
                       )
        cv2.drawMarker(img, (int(pred_iris_r[fn][0]), int(pred_iris_r[fn][1])), color=(0, 0, 255), markerType=cv2.MARKER_CROSS, markerSize=4,
                       thickness=1, line_type=cv2.LINE_AA
                       )

        # draw onecmo points
        cv2.drawMarker(img, (int(onecmo_l[fn][0]), int(onecmo_l[fn][1])), color=(0, 255, 255), markerType=cv2.MARKER_CROSS,
                       markerSize=4,
                       thickness=1, line_type=cv2.LINE_AA
                       )

        cv2.drawMarker(img, (int(onecmo_r[fn][0]), int(onecmo_r[fn][1])), color=(0, 255, 255), markerType=cv2.MARKER_CROSS,
                       markerSize=4,
                       thickness=1, line_type=cv2.LINE_AA
                       )

        # draw gaze origin and gaze end points for left eye
        poc = np.matmul(R_g_c, np.array(camera_data_l[fn][0]).reshape(3, 1)) + T_c
        pec = np.matmul(R_g_c, np.array(camera_data_l[fn][1]).reshape(3, 1)) + T_c
        kk = np.matmul(K_matrix, pec)
        kk = kk / kk[-1]
        x21, y21 = kk[0], kk[1]
        kk = np.matmul(K_matrix, poc)
        kk = kk / kk[-1]
        x1, y1 = kk[0], kk[1]
        alpha = 100
        end_point = poc + alpha * ((pec - poc) / np.linalg.norm((pec - poc)))
        kk = np.matmul(K_matrix, end_point)
        kk = kk / kk[-1]
        x2, y2 = kk[0], kk[1]

        cv2.drawMarker(img, (int(x1), int(y1)), color=(255, 255, 0), markerType=cv2.MARKER_CROSS, markerSize=4,
                       thickness=1, line_type=cv2.LINE_AA
                       )

        lineThickness = 1
        cv2.arrowedLine(img, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), lineThickness)

        # draw gaze origin and gaze end points for right eye
        poc = np.matmul(R_g_c, np.array(camera_data_r[fn][0]).reshape(3, 1)) + T_c
        pec = np.matmul(R_g_c, np.array(camera_data_r[fn][1]).reshape(3, 1)) + T_c
        kk = np.matmul(K_matrix, pec)
        kk = kk / kk[-1]
        x21, y21 = kk[0], kk[1]
        kk = np.matmul(K_matrix, poc)
        kk = kk / kk[-1]
        x1, y1 = kk[0], kk[1]
        alpha = 100
        end_point = poc + alpha * ((pec - poc) / np.linalg.norm((pec - poc)))
        kk = np.matmul(K_matrix, end_point)
        kk = kk / kk[-1]
        x2, y2 = kk[0], kk[1]

        cv2.drawMarker(img, (int(x1), int(y1)), color=(255, 255, 0), markerType=cv2.MARKER_CROSS, markerSize=4,
                       thickness=1, line_type=cv2.LINE_AA
                       )

        lineThickness = 1
        cv2.arrowedLine(img, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), lineThickness)


        cv2.imshow('img', img)
        cv2.waitKey(0)
        # cv2.imwrite(os.path.join(save_path, fn + '.jpg'), img)
